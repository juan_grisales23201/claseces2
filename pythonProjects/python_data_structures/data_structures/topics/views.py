from __future__ import annotations
from typing import Counter
from django.shortcuts import render
from .extras.tuplas import publicar, goles, jugador
from .extras.dataclases import jugadorDataClass



def cargar_index(request):
    return render(
        request,
        'topics/index.html',
        {}
    )
def cargar_tuplas(request):

    goles1 = ("liga 1", 1 , 8 ,0.12)
    nombre = "Leo Messi"

    j1 = jugador('champions', 4, 8, 0.5, 'Messi')

    return render(
        request,
        'topics/tuplas.html',
        {
            'd1': publicar(goles1,nombre),
            'd2': goles(goles1,nombre),
            'd3': j1

        }
    )

def cargar_dataclases(request):

    jugador = jugadorDataClass('champions', 3, 12, 0.25, 'Messi')
    jugador.casado = True
    return render(
        request,
        'topics/dataclases.html',
        {
            'j':jugador
        }
    )

def cargar_dictionaries(request):

    jugadores = {
        'jugador1': ("liga 1", 1 , 8 ,0.12, "Messi"),
        'jugador2': ("liga Italiana", 5 , 12 ,0.42 , "Usted"),
        'jugador3': ("liga Inglesa", 3 , 6 ,0.5 , "Claudio"),
        'jugador4': ("liga Española", 10 , 12 ,0.83 , "Marco")
    }

    

    class cualquierObjeto:
        def __init__(self, valor):
            self.valor = valor

    objeto1 = cualquierObjeto('Hola')

    jugadores['jugador10'] = ("Liga Alemana", 5, 10, 0.5, "Messi")

    cosas = {}
    cosas[objeto1] = 'Es un objeto'

    return render(
        request,
        'topics/dictionaries.html',
        {
            'j1':jugadores['jugador1'],
            'j2':jugadores['jugador2'],
            'j3':jugadores.get('jugador3', 'NOT_FOUND'),
            'j4': jugadores.setdefault('jugador1', 'INVALID'),
            'j5': jugadores.setdefault('jugador80', ('Sin definir', 0,0,0.0,'Sin definir')),
            'jugadores': jugadores,
            'o': objeto1
        }
    )

def cargar_lists(request):

    l1:list[str] = ['a', 'b', 'c', 'a']
    l2:list[int] = [12, 34, 56, 84, 12, 12]

    l1.append('d')
    l2.append(455)

    l1.insert(len(l1), 'z')
    l2.insert(0, 52)
    l1.reverse()
    l1.sort()
    return render(
        request,
        'topics/lists.html',
        {
            'l1': l1,
            'l2': l2,
            'c1': l1.count('a'),
            'c2':l2.count(12),
            'p': l1.index('b'),
            'r': l1
        }
    )

def cargar_sets(request):

    songs_library = [
        ('Phantom of the opera','Sarah Brightman'),
        ('cancieon 2','autor 2'),
        ('cancieon 3','autor 3'),
        ('cancieon 4','autor 4'),
        ('cancieon 5','autor 5'),
        ('cancieon 6','autor 6'),
    ]

    artists = set()

    for song, artist in songs_library:
        artists.add(artist)

    busqueda ='Sarah Brightman' in artists

    alphabetical = list(artists)
    alphabetical.sort()

    ana={
        'Sarah Brightman',
        'Opeth',
        'Vixy and Tony'
    }

    juan={
        'YESS',
        'Sarah Brightman',
        'Genesis'
    }

    operacion = ana.intersection(juan)
    


    return render(
        request,
        'topics/sets.html',
        {
            'a' : artists,
            'b':busqueda,
            'alphabetical':alphabetical,
            'operacion': operacion
        }
    )

def cargar_queues(request):
    return render(
        request,
        'topics/queues.html',
        {}
    )