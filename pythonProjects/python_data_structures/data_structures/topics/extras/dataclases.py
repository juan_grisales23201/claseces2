from dataclasses import dataclass

@dataclass(frozen=False, order=True)
class jugadorDataClass:
   
    competicion: str = ''
    goles: int = 0
    partidos: int = 0
    promedio: float = 0.0
    nombre: str = ''

    def obtenerGoles(self):
        return self.promedio*self.partidos 

    def mostrar(self):
        return f"{self.nombre} juega en {self.competicion}"