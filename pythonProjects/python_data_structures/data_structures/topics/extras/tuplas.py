
from typing import NamedTuple

def publicar(datos,nombre):
    competicion, goles, partidos, promedio = datos
    return f"{nombre} en {competicion} jugo {partidos} con un promedio de {promedio} goles por partido"

def goles(datos, nombre):
    goles = datos[1]

    return (nombre,goles)

class jugador(NamedTuple):

    competicion: str
    goles: int
    partidos: int
    promedio: float 
    nombre: str

    def obtenerGoles(self):
        return self.promedio*self.partidos 

    def mostrar(self):
        return f"{self.nombre} juega en {self.competicion}"