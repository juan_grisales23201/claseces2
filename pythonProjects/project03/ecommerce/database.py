from typing import Optional

class Database:

    def conectar(self) -> None:
        print("Conectado a la base de datos")

db: Optional[Database] = None

def inicializar_db():
    global db
    db = Database()

def get_db():
    global db
    if not db:
        db = Database()
        return db

if __name__ == "__main__":
    get_db()