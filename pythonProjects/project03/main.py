import math

from ecommerce.productos import Producto

from ecommerce.formas_pago.forma_pago_1 import transaccion

producto = Producto("Tablet")
producto.mostrar()
transaccion()

class Punto:
    """
    Representa un punto en las coordenadas geometricas
    en dos dimensiones.

    p0 = Punto()
    p1 = Punto(3,4)
    p0.distacias(p1) -> 5
    """

    def __init__(self, x: float=0, y: float=0) -> None:

        """
        Inicializa la posicion de un punto 'x' y 'y'
        que son coordenadas que se especificam
        Si no se ingresan puntos se estable por defecto

        :parametro x: float, coordenada x
        :parametro y: float, coordenada y
        """

        self.x = x
        self:y = y
    
    def resetear(self) -> None:

        """
        Resetea el punto de nuevo y lo pone en el origen: 0,0
        """

        self.x = 0.0
        self.y = 0.0

    def moverse(self, x: float, y: float) -> None:

        """
        Mueve el punto  a una ubicacion nueva en el espacio de 2D

        :parametro x: float, coordenada x
        :parametro y: float, coordenada y
        """

        self.x = x
        self.y = y

    def distancia(self, punto: "Punto") -> float:

        """
        Calcula la distancia Euclidiana desde el punto a 
        un segundo punto que se pasa como parametro

        :parametro punto: Una instacia de Punto
        :return: float, es la distacia
        """

        return math.hypot(self.x - punto.x, self.y - punto.y)


    