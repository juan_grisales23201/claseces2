import math
from mostrar import App
class Punto:

    def __init__(self, x: float =0, y: float= 0) -> None:
        self.x = x
        self.y = y
    
    def resetear(self):
        self.x = 0.0
        self.y = 0.0

    def moverse(self, x: float, y: float) -> None:
        self.x = x
        self.y = y

    def distancia(self, punto: "Punto") -> float:
        return math.hypot(self.x - punto.x, self.y - punto.y)

p1 = Punto(10,55)

app = App()
app.print(f"{p1.x}, {p1.y} \n\nHey whatsapp")

"""p2 = Punto(100,200)
app.print(f"({p2.x}, {p2.y})")"""



