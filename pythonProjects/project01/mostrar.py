import tkinter as tk
from tkinter import ttk
from tkinter.constants import NW
from typing import Text

class App:

    """ Clase para crear una ventana
    que muestra el texto de un print.t
    uso: ventana = App() """

    def __init__(self):
        """ Iniciar los componentes:
        la ventana, el contenedor, el texto, label1, btn_salir """

        self.ventana = tk.Tk()
        self.ventana.title("Programa 1")
        self.ventana.geometry("500x300")

        s = ttk.Style()
        s.configure('My.TFrame', background='#B9D3EE')

        self.contenedor = ttk.Frame(self.ventana, style='My.TFrame')
        self.contenedor.place(x=0, y=0, width=500, height=300)

        self.texto = tk.StringVar()
        self.label1 = ttk.Label(self.contenedor, textvariable=self.texto, anchor=NW, padding="10 10 10 10")
        self.label1.place(x=10, y=10, width=480,height=250)

        self.btn_salir = ttk.Button(self.contenedor, text="Salir",command=self.salir)
        self.btn_salir.place(x=415, y=270)

    def print(self, datos):
        """Para pasar el texto a mostrar.
        Uso: ventana.print("Hola mundo")"""
        self.texto.set(str(datos))
        self.ventana.mainloop() # Poner a funcionar la ventana

    def salir(self):
        """"Para salir de la ventana
        Lo llama el btn_salir"""
        self.ventana.destroy()

if __name__ == "__main__":
    app = App()
    app.print("This is so nice")

    
