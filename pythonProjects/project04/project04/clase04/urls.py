from django.urls import path
from .import views

app_name='clase04'

urlpatterns = {
    path('index/',views.cargar_index , name='index'),
    path('pagina1/',views.cargar_pagina1 , name='pagina1')
}