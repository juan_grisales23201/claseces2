from django.shortcuts import render
from .extras.clases import Contacto, vendedor, Amigo

def cargar_index(request):

    cantidad = 500
    lista = [1,2,3,4,5]

    c1 = Contacto('Juan', 'juan.grisales@gamil.com')
    c2 = Contacto('Jose', 'jose.@gamil.com')
    c3 = Contacto('maria', 'maria.grisales@gamil.com')
    todos=Contacto.contactos

    v1 = vendedor('mariana', 'mariana.perfumes@gamil.com')
    v1.ordenar("2 perfumes")
    return render(
        request,
        'clase04/index.html',
        {
            'x':cantidad,
            'l':lista,
            'c1':c1 ,
            'todos': todos,
            'v1': v1,
        }
    )

def cargar_pagina1(request):

    c1 = Contacto('juan', 'juan@gamil.com')
    c2 = Contacto('juan B', 'juan2@gamil.com')
    c3 = Contacto('carlos', 'carlos@gamil.com')

    contactos_juan = [ j.nombre for j in Contacto.contactos.buscar('juan ')]

    amigo =  Amigo('david', 'david11@gmail.com', '3225989564')

    todos = Contacto.contactos
    return render(
        request,
        'clase04/pagina1.html',
        {
            'hola':'hola que tal',
            'c1':c1,
            'c2':c2,
            'c3':c3,
            'contactos_juan': contactos_juan,
            'amigo':amigo,
            't':todos
        }
    )
