from typing import List

class ContactoList(List["Contacto"]):
    
    def buscar(self, nombre:str) ->List["Contacto"]:
        
        Contactos: List["Contacto"] = []

        for c in self:
            if nombre in c.nombre:
                Contactos.append(c)
        return Contactos

class Contacto:

    # contactos: List["Contacto"]=[]
    contactos = ContactoList()

    def __init__(self, nombre:str, email:str) -> None:
        self.nombre = nombre
        self.email = email

        if not self.existe():
            Contacto.contactos.append(self)

    def __repr__(self) -> str:
        return(f"{self.__class__.__name__}"
               f"({self.nombre}, {self.email})")

    def existe(self)-> bool:

        if len(Contacto.contactos):
            for c in Contacto.contactos :
                if self.nombre == c.nombre and self.email == c.email:
                    return True
        else:
            return False

class vendedor(Contacto):

    def ordenar(self, orden: str) -> str :
        self.orden = orden

    def consultar(self) -> str:
        return (f"Orden: {self.orden}"
               f"\n Vendedor: {self.nombre}"
                f"\nCorreo: {self.email}")

class Amigo(Contacto):

    def __init__(self, nombre: str, email: str,celular:str) -> None:
        super().__init__(nombre, email)
        self.celular = celular

    def __repr__(self) -> str:
        return(f"{self.__class__.__name__}"
               f"({self.nombre}, {self.email}, {self.celular})")

