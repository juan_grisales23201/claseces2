from django.apps import AppConfig


class Clase04Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'clase04'
